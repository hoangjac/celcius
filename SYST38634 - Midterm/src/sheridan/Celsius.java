package sheridan;

public class Celsius {

	public static int fromFahrenheit(int degree) {
		
		// Round up to nearest Celsius
		int celsius = (int)(Math.round((degree-32)*(5.0/9.0)));
		
		return celsius;
	}
}
