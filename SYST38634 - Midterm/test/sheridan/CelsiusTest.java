/*
 * @author Jack Hoang
 * Student ID: 991495093
 * Midterm Hands On
 * February 24th, 2021
 */

package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		
		int celsius = Celsius.fromFahrenheit(98);
		assertTrue("Error converting", celsius==37);
		
	}
	
	@Test
	public void testFromFahrenheitExceptional() {
		
		int celsius = Celsius.fromFahrenheit(-100);
		assertFalse("Error converting", celsius == -74);
		
	}
	
	@Test
	public void testFromFahrenheitBoundaryIn() {
		
		int celsius = Celsius.fromFahrenheit(60);
		assertTrue("Error converting", celsius == 16);
		
	}
	
	@Test
	public void testFromFahrenheitBoundaryOut() {
		
		int celsius = Celsius.fromFahrenheit(-1);
		assertFalse("Error converting", celsius == 19);
		
	}
	

}
